import React, { Component } from "react";

import BlackBox from "./blackBox";
import WhiteBox from "./whiteBlock";
import '../../css/grid.css'

class grid extends Component {
    state = {
        active_val: ""
    }

    setCurrentActiveVal = (val) => {
        if (this.state.active_val === val) {
            this.setState({ active_val: "" })
        } else {
            this.setState({ active_val: val })
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className="grid-wrapper">
                    {Array.apply(0, Array(8)).map((val, rowInd) => {
                        let grid_row = []
                        Array.apply(0, Array(8)).map((val, columnInd) => {
                            if (rowInd % 2 === 0 && columnInd % 2 === 0) {
                                grid_row.push(
                                <BlackBox 
                                    key={`${rowInd}_${columnInd}`} 
                                    postion={`${rowInd}_${columnInd}`} 
                                    currentActiveVal={this.state.active_val} 
                                    setCurrentActiveVal = {this.setCurrentActiveVal} 
                                />)
                            } else if (rowInd % 2 === 0 && columnInd % 2 !== 0) {
                                grid_row.push(
                                <WhiteBox 
                                    key={`${rowInd}_${columnInd}`} 
                                    postion={`${rowInd}_${columnInd}`} 
                                    currentActiveVal={this.state.active_val} 
                                    setCurrentActiveVal = {this.setCurrentActiveVal} 
                                />)
                            }
                            else if (rowInd % 2 !== 0 && columnInd % 2 === 0) {
                                grid_row.push(
                                    <WhiteBox key={`${rowInd}_${columnInd}`} 
                                    postion={`${rowInd}_${columnInd}`} 
                                    currentActiveVal={this.state.active_val} 
                                    setCurrentActiveVal = {this.setCurrentActiveVal} 
                                />)
                            }
                            else if (rowInd % 2 !== 0 && columnInd % 2 !== 0) {
                                grid_row.push(
                                    <BlackBox key={`${rowInd}_${columnInd}`} 
                                    postion={`${rowInd}_${columnInd}`} 
                                    currentActiveVal={this.state.active_val} 
                                    setCurrentActiveVal = {this.setCurrentActiveVal} 
                                />)
                            }
                        })
                        return grid_row
                    })}
                </div>
            </React.Fragment>
        );
    }
}

export default grid;