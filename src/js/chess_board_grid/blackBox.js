import React, { Component } from "react";

class BlackBox extends Component {

    render() {
        return (
            <button onClick={() => this.props.setCurrentActiveVal(this.props.postion)}>
                <div className={
                    this.props.currentActiveVal === this.props.postion ? "black_box active" : "black_box"
                }></div>
            </button>
        );
    }
}

export default BlackBox;