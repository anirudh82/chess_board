import React, { Component } from "react";
class WhiteBox extends Component {

    render() {
        return (
            <button onClick={() => this.props.setCurrentActiveVal(this.props.postion)}>
                <div className={
                    this.props.currentActiveVal === this.props.postion ? "white_box active" : "white_box"
                }>
                </div>
            </button>
        );
    }
}

export default WhiteBox;