import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import React from "react";
import './index.css'
import Grid from './js/chess_board_grid/grid'


function App() {
  return (
    <div className="App">
     <Router>
      <Routes>
        <Route path="/" element={<Grid />} />
      </Routes>
    </Router>

    </div>
  );
}

export default App;
